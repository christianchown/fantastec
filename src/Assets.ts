import {ImageRequireSource} from 'react-native';
import {IncidentType} from './Game';

const incidentImages: {[type in IncidentType]: ImageRequireSource} = {
  addedminutes: require('../assets/images/added-minutes.png'),
  away: require('../assets/images/crystalpalace.png'),
  camera: require('../assets/images/camera.png'),
  fulltime: require('../assets/images/full-time.png'),
  goal: require('../assets/images/goal.png'),
  halftime: require('../assets/images/half-time.png'),
  home: require('../assets/images/everton.png'),
  injury: require('../assets/images/injury.png'),
  kickoff: require('../assets/images/kick-off.png'),
  miss: require('../assets/images/missed-chance.png'),
  post: require('../assets/images/post.png'),
  save: require('../assets/images/save.png'),
  standard: require('../assets/images/standard.png'),
  sub: require('../assets/images/refresh.png'),
  whistle: require('../assets/images/whistle.png'),
  yellowcard: require('../assets/images/yellow-card.png'),
};

const homeImage: ImageRequireSource = require('../assets/images/everton-large.png');
const awayImage: ImageRequireSource = require('../assets/images/crystalpalace-large.png');

const titleFont = 'Rajdhani-SemiBold';
const standardFont = 'Montserrat-Regular';
const boldFont = 'Montserrat-SemiBold';

const green = '#81c200';
const greyText = '#55565a';
const grey = '#333333';
const lightGrey = '#cccccc';
const lighterGrey = '#e0e0e0';
const black = '#000000';
const white = '#FFFFFF';
const paleHighlight = 'rgba(184, 239, 73, 0.5)';

export {
  incidentImages,
  homeImage,
  awayImage,
  titleFont,
  standardFont,
  boldFont,
  green,
  greyText,
  lightGrey,
  lighterGrey,
  grey,
  black,
  white,
  paleHighlight,
};
