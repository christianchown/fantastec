import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {standardFont, greyText, boldFont, black} from './Assets';

interface BodyTextProps {
  text: string;
}

function Line({text}: BodyTextProps) {
  const segments = text.split('*');
  return (
    <View style={styles.paragraph}>
      <Text>
        {segments.map((segment, i) => (
          <Text
            key={`${text}segment${i}`}
            style={i % 2 === 0 ? styles.standard : styles.bold}>
            {segment}
          </Text>
        ))}
      </Text>
    </View>
  );
}

export default function BodyText({text}: BodyTextProps) {
  const paragraphs = text.split('\n');
  return (
    <View style={styles.container}>
      {paragraphs.map(line => (
        <Line key={line} text={line} />
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  paragraph: {
    marginBottom: 10,
  },
  standard: {
    fontFamily: standardFont,
    fontSize: 16,
    color: greyText,
  },
  bold: {
    fontFamily: boldFont,
    fontSize: 16,
    color: black,
  },
});
