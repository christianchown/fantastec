import React, {useCallback, useState, useEffect} from 'react';
import {Incident} from './Game';
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';

import KeyMoment from './KeyMoment';
import Arrow from './Arrow';
import {SeparatorDark} from './Separator';
import {titleFont, black, lighterGrey, lightGrey, grey} from './Assets';
import useLazyRef from './useLazyRef';

interface KeyMomentsProps {
  keyMoments: Incident[];
  scrollToIncident: (incident: Incident) => void;
}

const ICON_SIZE = 32;
const HEIGHT = 240;
const ICON_PADDING = 5;
export const HEADER_HEIGHT = ICON_SIZE + ICON_PADDING * 2;

export default function KeyMoments({
  keyMoments,
  scrollToIncident,
}: KeyMomentsProps) {
  const [hidden, setHidden] = useState(true);
  const y = useLazyRef(() => new Animated.Value(0 as number));
  const keyExtractor = useCallback((item: Incident) => item.id, []);
  const scrollTo = useCallback(
    (item: Incident) => () => scrollToIncident(item),
    [scrollToIncident],
  );
  const renderItem = useCallback(
    ({item}: {item: Incident}) => (
      <KeyMoment incident={item} onPress={scrollTo(item)} />
    ),
    [scrollTo],
  );
  const toggleHidden = useCallback(() => {
    setHidden(h => !h);
  }, []);
  useEffect(() => {
    Animated.timing(y, {
      toValue: hidden ? 0 : 1,
      useNativeDriver: true,
      easing: Easing.quad,
      duration: 350,
    }).start();
  }, [hidden, y]);
  const translateY = y.interpolate({
    inputRange: [0, 1],
    outputRange: [HEIGHT - HEADER_HEIGHT, 0],
  });
  const rotateZ = y.interpolate({
    inputRange: [0, 0.4, 0.6, 1],
    outputRange: [
      '180deg',
      hidden ? '0deg' : '180deg',
      hidden ? '0deg' : '180deg',
      '0deg',
    ],
  });

  return (
    <Animated.View style={[styles.container, {transform: [{translateY}]}]}>
      <View style={styles.header}>
        <Text style={styles.heading}>KEY MOMENTS</Text>
        <TouchableOpacity onPress={toggleHidden} activeOpacity={0.7}>
          <Animated.View style={{transform: [{rotateZ}]}}>
            <View style={styles.arrow}>
              <Arrow width={ICON_SIZE / 2} height={ICON_SIZE / 2} />
            </View>
          </Animated.View>
        </TouchableOpacity>
      </View>
      <FlatList
        style={styles.list}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        data={keyMoments}
        ItemSeparatorComponent={SeparatorDark}
      />
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: lightGrey,
    flex: 1,
    height: HEIGHT,
  },
  header: {
    flexDirection: 'row',
    paddingTop: ICON_PADDING,
    paddingBottom: ICON_PADDING,
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 10,
    borderBottomWidth: 1,
    borderBottomColor: grey,
  },
  arrow: {
    width: ICON_SIZE,
    height: ICON_SIZE,
    borderRadius: ICON_SIZE / 2,
    borderWidth: 2,
    borderColor: black,
    backgroundColor: lighterGrey,
    justifyContent: 'center',
    alignItems: 'center',
  },
  heading: {
    fontFamily: titleFont,
    textAlign: 'center',
    color: black,
    fontSize: 24,
    flex: 1,
  },
  list: {},
});
