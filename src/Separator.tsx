import React from 'react';
import {View, StyleSheet} from 'react-native';
import {lightGrey, grey} from './Assets';

export function SeparatorLight() {
  return <View style={[styles.separator, styles.light]} />;
}

export function SeparatorDark() {
  return <View style={[styles.separator, styles.dark]} />;
}

const styles = StyleSheet.create({
  separator: {
    marginLeft: 10,
    marginRight: 10,
    height: 1,
  },
  light: {
    backgroundColor: lightGrey,
  },
  dark: {
    backgroundColor: grey,
  },
});
