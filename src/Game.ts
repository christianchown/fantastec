import GameEvents from '../assets/commentary.json';

export interface Incident {
  id: string;
  type:
    | 'addedminutes'
    | 'away'
    | 'camera'
    | 'fulltime'
    | 'goal'
    | 'halftime'
    | 'home'
    | 'injury'
    | 'kickoff'
    | 'miss'
    | 'post'
    | 'save'
    | 'standard'
    | 'sub'
    | 'whistle'
    | 'yellowcard';
  time: string;
  headline: string;
  body: string;
  image?: string;
}

export type IncidentType = Incident['type'];

export function isKeyMoment({type}: Incident) {
  return (
    type === 'goal' ||
    type === 'yellowcard' ||
    type === 'post' ||
    type === 'injury' ||
    type === 'save'
  );
}

const incidents: Incident[] = GameEvents as Incident[];

export {incidents};
