import React, {useEffect, useMemo, useState, useCallback} from 'react';
import RNBootSplash from 'react-native-bootsplash';
import {StatusBar, SafeAreaView, View, StyleSheet} from 'react-native';

import {incidents, isKeyMoment, Incident} from './Game';
import Header from './Header';
import Commentary from './Commentary';
import KeyMoments from './KeyMoments';
import {green, white} from './Assets';

export default function App() {
  useEffect(() => {
    RNBootSplash.hide();
  }, []);
  const keyMoments = useMemo(() => incidents.filter(isKeyMoment), []);
  const [highlight, setHighlight] = useState<Incident | undefined>(undefined);
  const scrollToIncident = useCallback(
    (item: Incident) => setHighlight(item),
    [],
  );
  return (
    <View style={styles.wrap}>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={styles.container}>
        <View style={styles.header}>
          <Header home="EVERTON" away="CRYSTAL PALACE" />
        </View>
        <View style={styles.commentary}>
          <Commentary incidents={incidents} highlight={highlight} />
        </View>
        <View style={styles.keyMoments}>
          <KeyMoments
            keyMoments={keyMoments}
            scrollToIncident={scrollToIncident}
          />
        </View>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: green,
  },
  header: {},
  commentary: {
    flexGrow: 1,
    backgroundColor: white,
  },
  keyMoments: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
  },
});
