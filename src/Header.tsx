import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';

import {titleFont, homeImage, awayImage, greyText} from './Assets';

interface HeaderProps {
  home: string;
  away: string;
}

export default function Header({home, away}: HeaderProps) {
  return (
    <View style={styles.container}>
      <View style={styles.team}>
        <Image
          style={styles.teamImage}
          source={homeImage}
          resizeMode="contain"
        />
        <Text style={styles.teamName}>{home}</Text>
      </View>
      <View style={styles.team}>
        <Image style={styles.teamImage} source={awayImage} />
        <Text style={styles.teamName}>{away}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: 10,
    paddingBottom: 10,
  },
  team: {
    alignItems: 'center',
  },
  teamName: {
    fontFamily: titleFont,
    fontSize: 24,
    textAlign: 'center',
    color: greyText,
  },
  teamImage: {
    width: 80,
    height: 80,
  },
});
