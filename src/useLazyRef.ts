import {useRef} from 'react';

export default function useLazyRef<T>(creator: () => T) {
  const value = useRef<T>();
  if (!value.current) {
    value.current = creator();
  }
  return value.current;
}
