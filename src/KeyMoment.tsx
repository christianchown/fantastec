import React from 'react';
import {Incident} from './Game';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';

import {incidentImages, titleFont, standardFont, grey, black} from './Assets';

interface KeyMomentProps {
  incident: Incident;
  onPress: () => void;
}

export default function KeyMoment({
  incident: {type, headline, time},
  onPress,
}: KeyMomentProps) {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.6}>
      <View style={styles.container}>
        <Text style={styles.time}>{time}</Text>
        <Image
          style={styles.icon}
          source={incidentImages[type]}
          resizeMode="contain"
        />
        <Text style={styles.headline}>{headline}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
  },
  time: {
    fontFamily: titleFont,
    fontSize: 20,
    color: grey,
    flex: 1,
  },
  icon: {
    width: 40,
    height: 40,
    marginLeft: 10,
    marginRight: 10,
  },
  headline: {
    flex: 6,
    fontFamily: standardFont,
    fontSize: 14,
    color: black,
  },
});
