import React, {memo} from 'react';
import {Incident} from './Game';
import {View, StyleSheet, Text, Image} from 'react-native';

import {
  incidentImages,
  titleFont,
  black,
  boldFont,
  paleHighlight,
} from './Assets';
import BodyText from './BodyText';

interface CommentaryItemProps {
  incident: Incident;
  highlighted: boolean;
}

const CommentaryItem = memo(
  ({
    incident: {image, type, headline, body, time},
    highlighted,
  }: CommentaryItemProps) => {
    return (
      <View style={highlighted ? styles.highlight : undefined}>
        <View style={styles.container}>
          <View style={styles.info}>
            <Text style={styles.time}>{time}</Text>
            {type !== 'standard' && (
              <Image
                style={styles.icon}
                source={incidentImages[type]}
                resizeMode="contain"
              />
            )}
          </View>
          <View style={styles.details}>
            <Text style={styles.headline}>{headline}</Text>
            <BodyText text={body} />
            {image && (
              <Image
                source={{uri: image}}
                style={styles.image}
                resizeMode="contain"
              />
            )}
          </View>
        </View>
      </View>
    );
  },
);

export default CommentaryItem;

const styles = StyleSheet.create({
  highlight: {
    backgroundColor: paleHighlight,
  },
  container: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  info: {
    flex: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
  },
  time: {
    fontFamily: titleFont,
    fontSize: 30,
    color: black,
  },
  icon: {
    marginTop: 10,
    marginBottom: 10,
    width: 64,
    height: 64,
  },
  details: {
    flex: 7,
    paddingRight: 10,
  },
  headline: {
    fontFamily: boldFont,
    color: black,
    fontSize: 16,
    marginTop: 5,
    marginBottom: 5,
  },
  image: {
    width: '100%',
    height: 240,
    marginBottom: 10,
  },
});
