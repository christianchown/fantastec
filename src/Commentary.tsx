import React, {useCallback, useRef, useEffect} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';

import {Incident} from './Game';
import CommentaryItem from './CommentaryItem';
import {SeparatorLight} from './Separator';
import {HEADER_HEIGHT} from './KeyMoments';

interface CommentaryProps {
  incidents: Incident[];
  highlight?: Incident;
}

export default function Commentary({incidents, highlight}: CommentaryProps) {
  const keyExtractor = useCallback((item: Incident) => item.id, []);
  const renderItem = useCallback(
    ({item}: {item: Incident}) => (
      <CommentaryItem incident={item} highlighted={highlight === item} />
    ),
    [highlight],
  );
  const flatListRef = useRef<FlatList<Incident>>(null);
  useEffect(() => {
    if (highlight !== undefined && flatListRef.current) {
      flatListRef.current.scrollToIndex({
        animated: true,
        index: incidents.findIndex(i => i.id === highlight.id),
      });
    }
  }, [incidents, highlight]);
  const onScrollToIndexFailed = useCallback(() => {
    if (flatListRef.current) {
      flatListRef.current.scrollToEnd();
    }
  }, []);
  return (
    <View style={styles.container}>
      <FlatList
        ref={flatListRef}
        contentContainerStyle={styles.list}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        data={incidents}
        ListFooterComponent={View}
        ListFooterComponentStyle={styles.footer}
        ItemSeparatorComponent={SeparatorLight}
        onScrollToIndexFailed={onScrollToIndexFailed}
      />
    </View>
  );
}

const FOOTER_PADDING = 100;

const styles = StyleSheet.create({
  container: {},
  list: {},
  footer: {
    height: HEADER_HEIGHT + FOOTER_PADDING,
  },
});
